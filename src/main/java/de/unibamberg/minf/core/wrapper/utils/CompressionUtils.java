package de.unibamberg.minf.core.wrapper.utils;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@Slf4j
public class CompressionUtils {

    public static void extractGZipFile(String compressedFile, String extractedFile) throws IOException {
        byte[] buffer = new byte[1024];

        FileInputStream fileIn = new FileInputStream(compressedFile);
        InflaterInputStream inputStream = new GZIPInputStream(fileIn);
        FileOutputStream fileOutputStream = new FileOutputStream(extractedFile);

        int bytes_read;
        while ((bytes_read = inputStream.read(buffer)) > 0) {
            fileOutputStream.write(buffer, 0, bytes_read);
        }

        inputStream.close();
        fileOutputStream.close();
        fileIn.close();

        log.info("The file was extracted successfully!");
    }
    
    public static void extractBZ2File(String compressedFile, String extractedFile) throws IOException {
        FileInputStream fin = new FileInputStream(compressedFile);
        FileOutputStream fout = new FileOutputStream(extractedFile);
            
        BufferedInputStream in = new BufferedInputStream(fin);
        BufferedOutputStream out = new BufferedOutputStream(fout);
        
        BZip2CompressorInputStream bzIn = new BZip2CompressorInputStream(in);
        
        byte[] buffer = new byte[1024];
        int n;
        while ((n = bzIn.read(buffer)) > 0) {
            out.write(buffer, 0, n);
        }
        out.flush();
        
        fin.close();
        in.close();
        fout.close();
        out.close();
        bzIn.close();
        
        log.info("The file was extracted successfully!");
    }

    public static void extractTarGZFile(String compressedFile, String extractedFile) throws IOException {
        InputStream fileInputStream = new FileInputStream(compressedFile);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
        GzipCompressorInputStream gzipInputStream = new GzipCompressorInputStream(bufferedInputStream);
        TarArchiveInputStream tarInputStream = new TarArchiveInputStream(gzipInputStream);

        ArchiveEntry entry;
        while ((entry = tarInputStream.getNextEntry()) != null) {
            if (!tarInputStream.canReadEntryData(entry)) 
                continue;

            File outputFile = new File(extractedFile, entry.getName());
            if (entry.isDirectory()) {
                if (!outputFile.exists() && !outputFile.mkdirs()) {
                    throw new IOException("Failed to create directory: " + outputFile);
                }
            } else {
                OutputStream outputFileStream = new FileOutputStream(outputFile);
                BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputFileStream);
                byte[] buffer = new byte[1024];
                int len;
                while ((len = tarInputStream.read(buffer)) != -1) {
                    bufferedOutputStream.write(buffer, 0, len);
                }
                bufferedOutputStream.close(); 
                tarInputStream.close();
            }
        }
        log.info("The file was extracted successfully!");
    }

    public static void extractZipFile(String compressedFile) throws IOException {
        File destDir = new File("./");
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(compressedFile));
        ZipEntry zipEntry = zis.getNextEntry();

        while (zipEntry != null) {
            File newFile = newFile(destDir, zipEntry);
            if (zipEntry.isDirectory()) {
                if (!newFile.isDirectory() && !newFile.mkdirs())
                    throw new IOException("Failed to create directory " + newFile);
            } else {
                // fix for Windows-created archives
                File parent = newFile.getParentFile();
                if (!parent.isDirectory() && !parent.mkdirs()) {
                    throw new IOException("Failed to create directory " + parent);
                }

                // write file content
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
            }
            zipEntry = zis.getNextEntry();
        }

        zis.closeEntry();
        zis.close();
    }

    public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }
}
