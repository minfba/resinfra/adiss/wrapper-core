package de.unibamberg.minf.core.wrapper.utils;

import java.io.File;

public class DownloadUtils {

    // uses the tempdirectory for downloading files
    public static File getDownloadDir() {
		String filePath = System.getProperty("java.io.tmpdir");
		if (!filePath.endsWith(File.separator)) {
			filePath = filePath + File.separator;
		}
		return new File(filePath);
	}

    public static File getDownloadFilePath(String fileName) {
		File dir = getDownloadDir();
		return new File(dir, fileName);
	}

}
