package de.unibamberg.minf.core.wrapper.utils;

import java.text.SimpleDateFormat;
import java.util.Arrays;

public class Utils {

    // TODO these util functions can be further divided!

    public static String GetTimestamp() {
        return new SimpleDateFormat("yyyyMMddHHmm").format(new java.util.Date());
    }

    public static String extractIdFromUrl(String urlString) {
        // Assuming the ID is the last segment of the path
        String[] pathSegments = urlString.split("/");
        String id = pathSegments[pathSegments.length - 1];
        return id;
    }

    public static boolean stringContainsItemFromList(String inputStr, String[] items) {
        return Arrays.stream(items).anyMatch(inputStr::contains);
    }
    
}   
