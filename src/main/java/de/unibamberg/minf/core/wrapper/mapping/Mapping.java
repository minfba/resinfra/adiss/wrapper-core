package de.unibamberg.minf.core.wrapper.mapping;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;


@Slf4j
public final class Mapping {

    @JsonProperty("source")
    private final String source;

    @JsonProperty("target")
    private final String target;

    @JsonProperty("deleteSource")
    private final boolean deleteSource;

    @JsonProperty("exactPath")
    private final boolean exactPath;

    @JsonIgnore
    private String[] sourceArray;
    @JsonIgnore
    private String[] targetArray;

    private String[] getSourceArray() {
        if (sourceArray == null || sourceArray.length == 0)
            sourceArray = splitPathString(source);
        return sourceArray;
    }

    private String[] getTargetArray() {
        if ((targetArray == null || targetArray.length == 0) && target != null)
            targetArray = splitPathString(target);
        return targetArray;
    }

    private static String[] splitPathString(String path) {
        return splitPathString(path, true);
    }

    private static String[] splitPathString(String path, boolean keepRegex) {
        if (path.startsWith(".") || path.endsWith(".")) {
            log.error("Mapping path cannot start or end with '.'");
            return new String[0];
        }

        // split the path with "." but not if a "." is inside "{}"
        List<String> pathList = new ArrayList<>();
        int lastSplit = 0;
        boolean inBrackets = false;

        for (int i = 0; i < path.length(); i++) {
            char currentChar = path.charAt(i);

            if (currentChar == '{') {
                inBrackets = true;
            } else if (currentChar == '}') {
                inBrackets = false;
            }

            if (currentChar == '.' && !inBrackets) {
                String subString = path.substring(lastSplit, i);
                if (!keepRegex)
                    subString = subString.replace("{", "").replace("}", "");
                pathList.add(subString);
                lastSplit = i + 1;
            } else if (i == path.length() - 1) {
                String subString = path.substring(lastSplit, i + 1);
                if (!keepRegex)
                    subString = subString.replace("{", "").replace("}", "");
                pathList.add(subString);
            }
        }

        return pathList.toArray(new String[0]);
    }

    public Mapping() {
        this("", "");
    }

    public Mapping(String[] source, String[] target) {
        this(String.join(".", source), String.join(".", target), true, false);
    }

    public Mapping(String[] source, String[] target, boolean deleteSource, boolean exactPath) {
        this(String.join(".", source), String.join(".", target), deleteSource, exactPath);
    }

    public Mapping(String source, String target) {
        this(source, target, true, false);
    }

    public Mapping(String source, String target, boolean deleteSource, boolean exactPath) {
        this.source = source;
        this.target = target;
        this.sourceArray = splitPathString(source);
        this.targetArray = splitPathString(target);
        this.deleteSource = deleteSource;
        this.exactPath = exactPath;
    }

    public boolean equals(Mapping mapping) {
        if (mapping.source == source &&
                mapping.target == target &&
                mapping.deleteSource == deleteSource &&
                mapping.exactPath == exactPath)
            return true;
        return false;
    }

    public String toString() {
        return source + " --> " + target +
                "    deleteSource: " + this.deleteSource +
                "    exactPath: " + this.exactPath;
    }

    public static boolean MappingListContains(List<Mapping> mappings, Mapping mapping) {
        for (Mapping m : mappings) {
            if (m.equals(mapping)) {
                return true;
            }
        }
        return false;
    }

    public static boolean equalsMappingList(List<Mapping> mappings, List<Mapping> mappings2) {
        for (Mapping m : mappings) {
            boolean equal = false;
            for (Mapping m2 : mappings2) {
                if (m.equals(m2)) {
                    equal = true;
                    continue;
                }
            }
            if (!equal) {
                log.info("No equal mapping to \"" + m.toString() + "\" found!");
                return false;
            }
        }
        return true;
    }

    public static List<Mapping> GetMappingsFromJson(String filePath) {
        ObjectMapper objectMapper = new ObjectMapper();

        try (FileInputStream fileInputStream = new FileInputStream(filePath)) {
            TypeReference<List<Mapping>> typeReference = new TypeReference<List<Mapping>>() {
            };
            return objectMapper.readValue(fileInputStream, typeReference);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<String, Object> ReMap(Map<String, Object> entry, List<Mapping> mappings) {
        // map entries
        for (Mapping m : mappings) {
            
            String[] targetArray = m.getTargetArray();
            // if the target is wrapped in $ then input the value in the source path
            if (targetArray.length == 1 && targetArray[0].startsWith("$", 0) && targetArray[0].endsWith("$")) {
                String value = targetArray[0].substring(1, targetArray[0].length() - 1);
                entry = SetValueByPath(entry, m.getSourceArray(), value);
                continue;
            }

            List<String[]> sourcePaths = getRegexPaths(entry, m.source);

            for (String[] sourcePathArray : sourcePaths) {

                // if the mapping dictionary has null for target then remove the entry
                if (m.target == null) {
                    DeleteValueByPath(entry, sourcePathArray);
                    continue;
                }

                // is there a value for sourcePath?
                Object sourceValue = GetValueByPath(entry, sourcePathArray);

                if (sourceValue == null || (sourceValue instanceof Map && m.exactPath))
                    continue;

                String[] targetPath = targetArray.clone();
                if (targetPath[targetPath.length - 1].equals("*")) {
                    targetPath[targetPath.length - 1] = sourcePathArray[sourcePathArray.length - 1];
                }

                entry = SetValueByPath(entry, targetPath, sourceValue);

                if (m.deleteSource)
                    entry = DeleteValueByPath(entry, sourcePathArray);

            }
        }
        return entry;
    }

    public static Map<String, Object> DeleteValueByPath(Map<String, Object> entry, String path) {
        return DeleteValueByPath(entry, splitPathString(path, false));
    }

    public static Map<String, Object> DeleteValueByPath(Map<String, Object> entry, String[] path) {
        if (path.length == 1) {
            entry.remove(path[0]);
            return entry;
        }

        Object nextEntry = entry.get(path[0]);
        // check if a value exists that is not a map
        if (nextEntry instanceof Map) {
            DeleteValueByPath((Map<String, Object>) nextEntry, Arrays.copyOfRange(path, 1, path.length));
        } else if (nextEntry instanceof List) {
            // again dnb case: when map-entry is inside a list
            List<?> list = (List<?>) entry.get(path[0]);
            for (Object item : list) {
                if (item instanceof Map && ((Map) item).containsKey(path[1])) {
                    DeleteValueByPath((Map<String, Object>) item, Arrays.copyOfRange(path, 1, path.length));
                }
            }
            // delete empty maps from list:
            list.removeIf(o -> o instanceof Map && ((Map<?, ?>) o).isEmpty());
        }

        return entry;
    }

    public static Map<String, Object> SetValueByPath(Map<String, Object> entry, String path, Object value) {
        return SetValueByPath(entry, splitPathString(path, false), value);
    }

    public static Map<String, Object> SetValueByPath(Map<String, Object> entry, String[] path, Object value) {
        if (path.length == 1) {
            // Check if the value exists
            if (entry.containsKey(path[0])) {
                Object existingValue = entry.get(path[0]);
                if (existingValue instanceof Map) {
                    // Don't add if it is a Map (Workaround for specific datasets)
                    entry.put(path[0], value);
                    return entry;
                }

                List<Object> newEntry = new ArrayList<>();
                if (existingValue instanceof List) {
                    newEntry.addAll((List<Object>) existingValue);
                } else {
                    newEntry.add(existingValue);
                }

                if (value instanceof List) {
                    newEntry.addAll((List<Object>) value);
                } else {
                    newEntry.add(value);
                }

                entry.put(path[0], newEntry);
            } else {
                entry.put(path[0], value);
            }
            return entry;
        }

        if (entry.containsKey(path[0]) && !(entry.get(path[0]) instanceof Map)) {
            // create a map and add the value
            Map<String, Object> newEntry = new HashMap<>();
            newEntry.put("value", entry.get(path[0]));
            entry.put(path[0], newEntry);
        }
        entry.computeIfAbsent(path[0], k -> new HashMap<String, Object>());
        entry.put(path[0], SetValueByPath((Map<String, Object>) entry.get(path[0]), Arrays.copyOfRange(path, 1, path.length), value));

        return entry;
    }

    public static Object GetValueByPath(Map<String, Object> entry, String path) {
        return GetValueByPath(entry, splitPathString(path, false));
    }

    public static Object GetValueByPath(Map<String, Object> entry, String[] path) {
        Object subEntry = entry;

        for (int i = 0; i < path.length; i++) {
            if (i == path.length - 1) {
                // Handle the case where the value is in a list of maps
                if (subEntry instanceof List<?> list && !list.isEmpty() && list.get(0) instanceof Map<?, ?> map) {
                    if (!map.containsKey(path[i])) {
                        return null;
                    }

                    List<Object> listVals = new ArrayList<>();
                    for (Object item : list) {
                        if (item instanceof Map<?, ?>) {
                            listVals.add(((Map<?, ?>) item).get(path[i]));
                        } else {
                            listVals.add(item);
                        }
                    }
                    return listVals;
                } else if (subEntry instanceof Map<?, ?> map) {
                    return map.get(path[i]);
                } else {
                    return null;
                }
            }

            if (subEntry instanceof Map<?, ?> map && map.containsKey(path[i])) {
                subEntry = map.get(path[i]);
            } else {
                return null;
            }
        }

        return null;
    }

    private static List<String> getRegexMatches(String regex, String[] keys) {
        List<String> matches = new ArrayList<>();
        for (String key : keys) {
            if (key.matches(regex))
                matches.add(key);
        }
        return matches;
    }

    public static List<String[]> getRegexPaths(Map<String, Object> entry, String path) {
        return getRegexPaths(entry, splitPathString(path));
    }

    public static List<String[]> getRegexPaths(Map<String, Object> entry, String[] path) {
        List<String[]> sourcePaths = new ArrayList<>();
        if (path.length == 0) {
            return new ArrayList<>();
        } else if (path.length == 1) {
            if (path[0].startsWith("{") && path[0].endsWith("}")) { // regex!!!
                String regex = path[0].substring(1, path[0].length() - 1);
                List<String> matches = getRegexMatches(regex, entry.keySet().toArray(new String[0]));
                for (String match : matches)
                    sourcePaths.add(new String[] { match });
                return sourcePaths;
            } else if (entry.containsKey(path[0])) {
                sourcePaths.add(path);
                return sourcePaths;
            } else {
                return sourcePaths;
            }

        } else {

            List<String> matches = new ArrayList<>();
            if (path[0].startsWith("{") && path[0].endsWith("}")) { // regex!!!
                String regex = path[0].replace("{", "").replace("}", "");
                matches = getRegexMatches(regex, entry.keySet().toArray(new String[0]));
            } else if (entry.containsKey(path[0])) {
                matches.add(path[0]);
            }

            for (String match : matches) {
                Object value = entry.get(match);
                if (!(value instanceof Map) &&
                        !(value instanceof List && !((List<?>) value).isEmpty()
                                && ((List<?>) value).get(0) instanceof Map))
                    continue;

                List<String[]> subPaths = new ArrayList<>();
                if (value instanceof List)
                    subPaths = getRegexPaths((Map<String, Object>) ((List<?>) value).get(0),
                            Arrays.copyOfRange(path, 1, path.length));
                else if (value instanceof Map)
                    subPaths = getRegexPaths((Map<String, Object>) value, Arrays.copyOfRange(path, 1, path.length));

                for (String[] subPath : subPaths) {
                    String[] newPath = new String[subPath.length + 1];
                    newPath[0] = match;
                    System.arraycopy(subPath, 0, newPath, 1, subPath.length);
                    sourcePaths.add(newPath);
                }

            }
            return sourcePaths;
        }

    }


}