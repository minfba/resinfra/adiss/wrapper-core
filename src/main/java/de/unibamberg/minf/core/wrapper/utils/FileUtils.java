package de.unibamberg.minf.core.wrapper.utils;

import java.io.File;
import java.io.FilenameFilter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileUtils {
    
    public static void DeleteFile(String filePath) {
        File file = new File(filePath);
        if (file.isFile()) {
            file.delete();
            log.info("File {} deleted", filePath);
        }
    }

    public static void DeleteDirectory(String dirPath) {
        File file = new File(dirPath);
        if (file.isDirectory()) {
            file.delete();
            log.info("Directory {} deleted", dirPath);
        }
    }

    public static void DeleteFilesInDir(String dirPath, String... suffixes) {
        File dir = new File("./");
        File[] files = dir.listFiles(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                for (String suffix : suffixes) {
                    if (name.toLowerCase().endsWith(suffix.toLowerCase())) {
                        return true;
                    }
                }
                return false;
            }
        });
        for(File f : files) 
            f.delete();
        log.info("{} files in directory {} deleted", files.length, dirPath);
    }

}
