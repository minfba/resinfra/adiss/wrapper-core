package de.unibamberg.minf.core.wrapper.utils;


import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.PrecisionModel;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;
import org.locationtech.jts.io.geojson.GeoJsonWriter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GeoUtils {

    
    private static WKTReader wktReader = new WKTReader();

    public static String wktToGeoJson(String wktLiteral) {
        return wktToGeoJson(wktLiteral, false);
    }


    public static String wktToGeoJson(String wktLiteral, boolean isForceCCW) {
        GeoJsonWriter geojsonWriter = new GeoJsonWriter();
        geojsonWriter.setForceCCW(isForceCCW);
        
        if (wktLiteral != null) {
            Geometry g;
            try {
                g = wktReader.read(wktLiteral);
                g.setSRID(4326);
                String geoJsonString = geojsonWriter.write(g);
                return geoJsonString;
            } catch (ParseException e) {
                log.error("Error parsing WKT: " + wktLiteral, e);
            }
            
        } 
        return null;
    }

    public static String latLonStringToGeoJson(String latlon) {
        if (latlon != null) {
            String[] latlonArray = latlon.split(",");
            if (latlonArray.length == 2) {
                String lat = latlonArray[0];
                String lon = latlonArray[1];
                String wkt = "Point ( " + lon + " " + lat + " )";
                return wktToGeoJson(wkt);
            }
        }
        return null;
    }

    public static void main(String[] args) {
        String wktString = "Polygon (( +041.243611 +039.908611, +041.243611 +039.908611, +041.276944 +039.908611, +041.276944 +039.908611, +041.243611 +039.908611 ))";
        String geoJsonString = wktToGeoJson(wktString);
        System.out.println(geoJsonString);
        String geoJsonString2 = wktToGeoJson(wktString, true);
        System.out.println(geoJsonString2);

    }

}
