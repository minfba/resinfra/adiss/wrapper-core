package de.unibamberg.minf.core.wrapper;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Test;

import de.unibamberg.minf.core.wrapper.mapping.Mapping;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MappingTest {
    
    @Test
    void testInitializations() {
        String source1 = "s1.s2";
        String target1 = "t1.t2";
        Mapping m1 = new Mapping(source1, target1);

        String source2 = "s2.s1";
        String target2 = "t1.t2";
        Mapping m2 = new Mapping(source2, target2);

        assertFalse(m1.equals(m2));
    }

    @Test
    void testInitializationsFromFile() {
        List<Mapping> mappings = Mapping.GetMappingsFromJson("mapping_test.json");
    }

    

}
